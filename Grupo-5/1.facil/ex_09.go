package main

import "fmt"

func main() {
	var n1, n2, n3 int
	fmt.Print("Dê-me um número: ")
	fmt.Scan(&n1)
	fmt.Print("Dê-me outro número: ")
	fmt.Scan(&n2)
	fmt.Print("Dê-me um último número: ")
	fmt.Scan(&n3)
	if (n1 > n2) && (n1 > n3) {
		fmt.Printf("O maior número é %d", n1)
	} else if n2 > n1 && n2 > n3 {
		fmt.Printf("O maior número é %d", n2)
	} else if n3 > n1 && n3 > n2 {
		fmt.Printf("O maior número é %d", n3)
	}

}
