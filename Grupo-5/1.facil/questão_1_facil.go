package main

import "fmt"

func soma(x int, y int) {
	fmt.Print("soma dos dois valores: ", x+y)
}

func main() {
	var x, y int
	fmt.Print("digite o primeiro valor: ")
	fmt.Scan(&x)
	fmt.Print("digite o segundo valor: ")
	fmt.Scan(&y)
	soma(x, y)
}
